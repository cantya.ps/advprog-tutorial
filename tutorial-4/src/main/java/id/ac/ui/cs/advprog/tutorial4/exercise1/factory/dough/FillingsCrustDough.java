package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FillingsCrustDough implements Dough {
    public String toString() {
        return "Fillings Crust style dough";
    }
}
