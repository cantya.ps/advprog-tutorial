package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class MayoSauce implements Sauce {
    public String toString() {
        return "Mayo Sauce";
    }
}
