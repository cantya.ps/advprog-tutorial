package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        // TODO Complete me!
        // Create a new Pizza Store franchise at Depok
        PizzaStore depokStore = new DepokPizzaStore();

        Pizza pizzas = depokStore.orderPizza("cheese");
        System.out.println("Anastasia ordered a " + pizzas + "\n");

        pizzas = depokStore.orderPizza("clam");
        System.out.println("Niklas ordered a " + pizzas + "\n");

        pizzas = depokStore.orderPizza("veggie");
        System.out.println("Max ordered a " + pizzas + "\n");
    }
}
