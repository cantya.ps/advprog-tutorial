package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    private static Singleton uniqueInstances = new Singleton();

    private Singleton(){}

    public static Singleton getInstance() {
        // TODO Implement me!
        if(uniqueInstances == null){
            uniqueInstances = new Singleton();
        }
        return uniqueInstances;
    }
}
